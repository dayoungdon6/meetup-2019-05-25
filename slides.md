# Parauapebas
# Tech Day

25/05/2019

----  ----

## Recados

----  ----

## Para Receber o Certificado

https://www.even3.com.br/pbstechday/

----  ----

Se Inscreva Nas Atividades

----  ----

<!-- .slide: data-background-image="images/subscribe0.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->


----  ----

Já está inscrito?  
Não tem problema!

----  ----

<!-- .slide: data-background-image="images/subscribe1.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----  ----

Programação > Realizar inscrição > Finalizar compra

----  ----

<!-- .slide: data-background-image="images/subscribe2.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

<img src="images/subscribe2-1.png" alt="Finalizar compra"> <!-- .element: class="fragment" -->

----  ----

Certifique-se da inscrição

----  ----

<!-- .slide: data-background-image="images/subscribe3.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----  ----

Facilite seu credenciamento

----  ----

<!-- .slide: data-background-image="images/subscribe4.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----  ----

## O Que Temos Para Hoje

* Abertura - Jorge Clécio
* Primeiros passos com Docker - Dhony Silva e Tiago Rocha
* O que é o DevSecOps - Thiago Rodrigues
* SCRUM - Hélio Bentzen
* Desenvolvimento nativo para Android com Adroid Studio - Willian Begot

----  ----

<a href="https://www.meetup.com/devopspbs/"><img width="240" data-src="images/DEVOLPBS.png" alt="DevOpsPBS"></a>

Grupo de Entusiastas, Estudantes e Profissionais de TI de Parauapebas (Desenvolvedores, Sysadmins, Infosecs, QAs, Engs/Admins de rede, Técnicos de informática, Analistas, Web Designers, etc)

----  ----

## Conduta

"Participe de maneira autêntica e ativa. Ao fazer isso, você contribui para a saúde e a longevidade dessa comunidade."

[//]: # (https://garoa.net.br/wiki/C%C3%B3digo_de_Conduta_Completo)

----  ----

## Site e Mídias Sociais

* **Site oficial**
  - Em breve (gostaria de ajudar?)

* **Mídias sociais**
  - [meetup.com/devopspbs](https://www.meetup.com/devopspbs/)
  - [gitlab.com/devopspbs](https://gitlab.com/devopspbs)

* **Grupos de discussão**
  - Grupo [DevOpsPBS](https://t.me/joinchat/A-G57xd_fjAnQwOzV_sdeQ) no Telegram
  - Grupo [TECNOLOGIA PBS](https://chat.whatsapp.com/7XfTiu53icSKKbANTQnMGH) no Whatsapp

----  ----

## Patrocínio:

<a href="https://jupiter.com.br/"><img height="160" data-src="images/logotipo-midia-digital-fundo-branco.svg" alt="Júpiter"></a>

## Apoio:

<a href="http://www.parauapebas.pa.gov.br/"><img height="90" data-src="images/parauapebas.svg" alt="Parauapebas"></a>
&emsp; <a href="https://parauapebas.ifpa.edu.br/"><img height="90" data-src="images/IFPA.svg" alt="IFPA"></a>

----  ----

## Dica: Linkedin Mobile

![](images/linkedin.png)
